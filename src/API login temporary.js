import React, { Component } from 'react';

class App extends Component {

componentDidMount() {
(function() {
    
    function login(callback) {
        var CLIENT_ID = '03d11eee5e8247bdb15ad1ba08b1fee4';
        var REDIRECT_URI = 'http://localhost:3000/callback.html';
        function getLoginURL(scopes) {
            return 'https://accounts.spotify.com/authorize?client_id=' + CLIENT_ID +
                '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
                '&scope=' + encodeURIComponent(scopes.join(' ')) +
                '&response_type=token';
        }
        
        var url = getLoginURL([
            'user-read-private',
            'playlist-read-private',
            'playlist-modify-public',
            'playlist-modify-private',
            'user-library-read',
            'user-library-modify',
            'user-follow-read',
            'user-follow-modify'
        ]);
        
        var width = 450,
            height = 730,
            left = (window.screen.width / 2) - (width / 2),
            top = (window.screen.height / 2) - (height / 2);
    
        window.addEventListener("message", function(event) {
            var hash;
            try {
                hash = JSON.parse(event.data);
                if (hash.type === 'access_token') {
                    callback(hash.access_token);
                }
            } catch (error) {
                return null;
            }         
        }, false);
        
        window.open(
            url,
            'Spotify',
            'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
        );  
        
    }

    function getUserData(accessToken) {
        var myHeaders = new Headers();

        myHeaders.append("Authorization", 'Bearer ' + accessToken);

        var myInit = { 
            method: 'GET',
            headers: myHeaders,
            mode: 'cors',
            cache: 'default' 
        };

        return fetch('https://api.spotify.com/v1/me', myInit)
    }

    var loginButton = document.getElementById('btn-login');
    var result = []    
    
    loginButton.addEventListener('click', function() {
        login(function(accessToken) {
            getUserData(accessToken)
                .then(function(response) {
                    loginButton.style.display = 'none';
                    return response.json();
                })
                .then(res => {
                    Object.keys(res).map(el => result.push(el + res[el]))
                    document.getElementById('avatar').src = res.images[0].url
                    document.getElementById('result').innerHTML += result
                })
            });
    });
    
})();
}

  render() {
    return (
      <div className="App">
            <button className="btn btn-primary" id="btn-login">Login</button>
            <div id="result">
                <img id="avatar" src="" alt="" height="100"/>
            </div>
      </div>
    );
  }
}

export default App;
