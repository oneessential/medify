import ActionTypes from '../constants';
import { showLoader, hideLoader } from './loader';
import { get } from '../utils/fetch';

export {
  getAlbum,
  getAlbumTracks
};

function getAlbumSuccess(data) {
  return {
    type: ActionTypes.GET_ALBUM_SUCCESS,
    payload: data
  };
}

function getAlbumError(error) {
  return {
    type: ActionTypes.GET_ALBUM_ERROR,
    payload: {
        error
    }
  };
}

function getAlbum(albumId) {
    const API = ActionTypes.API_BASE + 'albums/' + encodeURIComponent(albumId)

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getAlbumSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getAlbumError(text)));
            })
    }
}

function getAlbumTracksSuccess(data) {
  return {
    type: ActionTypes.GET_ALBUM_TRACKS_SUCCESS,
    payload: data
  };
}

function getAlbumTracksError(error) {
  return {
    type: ActionTypes.GET_ALBUM_TRACKS_ERROR,
    payload: {
        error
    }
  };
}

function getAlbumTracks(albumId) {
    const API = ActionTypes.API_BASE + 'albums/' + encodeURIComponent(albumId) + '/tracks'

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getAlbumTracksSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getAlbumTracksError(text)));
            })
    }
}