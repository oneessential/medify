import ActionTypes from '../constants';
import { showLoader, hideLoader } from './loader';
import { get } from '../utils/fetch';

export {
  getArtist,
  getArtistTopTracks,
  getArtistAlbums
};

function getArtistSuccess(artist) {
  return {
    type: ActionTypes.GET_ARTIST_SUCCESS,
    payload: artist
  };
}

function getArtistError(error) {
  return {
    type: ActionTypes.GET_ARTIST_ERROR,
    payload: {
        error
    }
  };
}

function getArtist(artistId) {
    const API = ActionTypes.API_BASE + 'artists/' + artistId;

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getArtistSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getArtistError(text)));
            })
    }
}

function getArtistTopTracksSuccess(tracks) {
  return {
    type: ActionTypes.GET_ARTIST_TOP_TRACKS_SUCCESS,
    payload: tracks
  };
}

function getArtistTopTracks(artistId) {
    const API = ActionTypes.API_BASE + 'artists/' + artistId + '/top-tracks?country=' + encodeURIComponent('GB')

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getArtistTopTracksSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getArtistError(text)));
            })
    }
}

function getArtistAlbumsSuccess(tracks) {
  return {
    type: ActionTypes.GET_ARTIST_ALBUMS_SUCCESS,
    payload: tracks
  };
}

function getArtistAlbums(artistId) {
    const API = ActionTypes.API_BASE + 'artists/' + artistId + '/albums?country=' + encodeURIComponent('GB')

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getArtistAlbumsSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getArtistError(text)));
            })
    }
}
