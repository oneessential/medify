import ActionTypes from '../constants';
import { showLoader, hideLoader } from './loader';
import { get } from '../utils/fetch';

export {
  getNewReleases,
  getFeaturedPlaylists,
  getBrowseCategories,
  getBrowseCategoryPlaylists
};

function getNewReleasesSuccess(data) {
  return {
    type: ActionTypes.GET_NEW_RELEASES_SUCCESS,
    payload: data
  };
}

function getNewReleasesError(error) {
  return {
    type: ActionTypes.GET_NEW_RELEASES_ERROR,
    payload: {
        error
    }
  };
}

function getNewReleases(country = 'GB') {
    const API = ActionTypes.API_BASE + 'browse/new-releases?country=' + encodeURIComponent(country)

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getNewReleasesSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getNewReleasesError(text)));
            })
    }
}

function getFeaturedPlaylistsSuccess(data) {
  return {
    type: ActionTypes.GET_FEATURED_PLAYLISTS_SUCCESS,
    payload: data
  };
}

function getFeaturedPlaylistsError(error) {
  return {
    type: ActionTypes.GET_FEATURED_PLAYLISTS_ERROR,
    payload: {
        error
    }
  };
}

function getFeaturedPlaylists(country = 'GB', timestamp) {
    const API = ActionTypes.API_BASE + 'browse/featured-playlists?country=' +
					encodeURIComponent(country)
                    //  +
					// '&timestamp=' + encodeURIComponent(timestamp)

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getFeaturedPlaylistsSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getFeaturedPlaylistsError(text)));
            })
    }
}

function getBrowseCategoriesSuccess(data) {
  return {
    type: ActionTypes.GET_BROWSE_CATEGORIES_SUCCESS,
    payload: data
  };
}

function getBrowseCategoriesError(error) {
  return {
    type: ActionTypes.GET_BROWSE_CATEGORIES_ERROR,
    payload: {
        error
    }
  };
}

function getBrowseCategories() {
    const API = ActionTypes.API_BASE + 'browse/categories'

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getBrowseCategoriesSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getBrowseCategoriesError(text)));
            })
    }
}

function getBrowseCategoryPlaylistsSuccess(data) {
  return {
    type: ActionTypes.GET_BROWSE_CATEGORY_PLAYLISTS_SUCCESS,
    payload: data
  };
}

function getBrowseCategoryPlaylistsError(error) {
  return {
    type: ActionTypes.GET_BROWSE_CATEGORY_PLAYLISTS_ERROR,
    payload: {
        error
    }
  };
}

function getBrowseCategoryPlaylists(categoryId, country = 'GB') {
    const API = ActionTypes.API_BASE + 'browse/categories/' + categoryId + '/playlists?country=' + encodeURIComponent(country)

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getBrowseCategoryPlaylistsSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getBrowseCategoryPlaylistsError(text)));
            })
    }
}