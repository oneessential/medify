import ActionTypes from '../constants';

export {
  showLoader,
  hideLoader
};

function showLoader() {
  return {
    type: ActionTypes.SHOW_LOADER,
    payload: true
  };
}

function hideLoader() {
  return {
    type: ActionTypes.HIDE_LOADER,
    payload: false
  };
}