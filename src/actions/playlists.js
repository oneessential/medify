import ActionTypes from '../constants';
import { showLoader, hideLoader } from './loader';
import { get } from '../utils/fetch';

export {
  getPlaylists,
  getPlaylist,
  getTrack
};

function getPlaylistsSuccess(data) {
  return {
    type: ActionTypes.GET_PLAYLISTS_SUCCESS,
    payload: data
  };
}

function getPlaylistsError(error) {
  return {
    type: ActionTypes.GET_PLAYLISTS_ERROR,
    payload: {
        error
    }
  };
}

function getPlaylists(username) {
    const API = ActionTypes.API_BASE + 'users/' + encodeURIComponent(username) + '/playlists'

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getPlaylistsSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getPlaylistsError(text)));
            })
    }
}

function getPlaylistSuccess(data) {
  return {
    type: ActionTypes.GET_PLAYLIST_SUCCESS,
    payload: data
  };
}

function getPlaylistError(error) {
  return {
    type: ActionTypes.GET_PLAYLIST_ERROR,
    payload: {
        error
    }
  };
}

function getPlaylist(username, playlistId) {
    const API = ActionTypes.API_BASE + 'users/' + encodeURIComponent(username) + '/playlists/' + encodeURIComponent(playlistId)

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getPlaylistSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getPlaylistError(text)));
            })
    }
}

function getTrackSuccess(data) {
  return {
    type: ActionTypes.GET_TRACK_SUCCESS,
    payload: data
  };
}

function getTrackError(error) {
  return {
    type: ActionTypes.GET_TRACK_ERROR,
    payload: {
        error
    }
  };
}

function getTrack(trackId) {
    const API = ActionTypes.API_BASE + '/tracks/' + encodeURIComponent(trackId)

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getTrackSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getTrackError(text)));
            })
    }
}