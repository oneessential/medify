import ActionTypes from '../constants';
import { showLoader, hideLoader } from './loader';
import { get } from '../utils/fetch';

export {
  getMe,
  getMyPlaylists,
  getUser
};

function getMeSuccess(data) {
  return {
    type: ActionTypes.GET_ME_SUCCESS,
    payload: data
  };
}

function getMeError(error) {
  return {
    type: ActionTypes.GET_ME_ERROR,
    payload: {
        error
    }
  };
}

function getMe() {
    const API = ActionTypes.API_BASE + 'me';

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getMeSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getMeError(text)));
            })
    }
}

function getMyPlaylistsSuccess(data) {
  return {
    type: ActionTypes.GET_MY_PLAYLISTS_SUCCESS,
    payload: data
  };
}

function getMyPlaylistsError(error) {
  return {
    type: ActionTypes.GET_MY_PLAYLISTS_ERROR,
    payload: {
        error
    }
  };
}

function getMyPlaylists() {
    const API = ActionTypes.API_BASE + 'me/playlists';

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getMyPlaylistsSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getMyPlaylistsError(text)));
            })
    }
}

function getUserSuccess(data) {
  return {
    type: ActionTypes.GET_USER_SUCCESS,
    payload: data
  };
}

function getUserError(error) {
  return {
    type: ActionTypes.GET_USER_ERROR,
    payload: {
        error
    }
  };
}

function getUser(username) {
    const API = ActionTypes.API_BASE + 'users/' + encodeURIComponent(username);

    return dispatch => {
        dispatch(showLoader());
        return get(API)
            .then(response => {
                if(response.status !== 200) return Promise.reject(response);

                return response.json()
                    .then(data => {
                        dispatch(hideLoader());
                        return dispatch(getUserSuccess(data));
                    });
            })
            .catch(error => {
                dispatch(hideLoader());

                return error.text()
                    .then(text => dispatch(getUserError(text)));
            })
    }
}
