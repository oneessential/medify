import React from 'react';
import { Link } from 'react-router-dom';

const Cover = ({ id, image, name, link }) => {

  return (
    <li key={ id }>
      <Link to={ link } style={{ backgroundImage: 'url(' + image + ')'}} >
        {/*<img src={ image } alt={ name }/>*/}
      </Link>
      <p>
        <Link to={ link }>{ name }</Link>
      </p>
    </li>
  );
};

export default Cover;