import React from 'react';
import { Link } from 'react-router-dom';

const Profile = ({ image, name, type, description, followers, author, authorLink }) => {

  return (
    <header className="profile">
      <div className="cover">
        <img src={ image } alt={ name }/>
      </div>
      <div className="info">
        <span className="type">{ type.toLocaleUpperCase() }</span>
        <h1 className="name">{ name }</h1>
        { description && <p className="description">{ description }</p> }
        { followers && <p className="followers">{ followers.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") } followers</p> }
        { authorLink && <p className="authorLink">
          Author: <Link to={ authorLink }>{ author }</Link>
        </p> }
      </div>
    </header>
  );
};

export default Profile;