import React from 'react';
import { Link } from 'react-router-dom';
import { formatDuration } from '../../utils/duration';

const Track = ({ id, index, name, albumName, artist, link, duration, preview_url, artistLink }) => {

  return (
    <tr key={ id }>
      <td>{ index + 1 }</td>
      { preview_url ? <td><Link to={ preview_url } target="_blank">{ name }</Link></td> : <td>{ name }</td>}
      { artist && <td><Link to={ artistLink }>{ artist }</Link></td>}
      <td>{ formatDuration(duration) }</td>
      { albumName && <td><Link to={ link }>{ albumName }</Link></td> }
    </tr>
  );
};

export default Track;