const actionTypes = {
  API_BASE: 'https://api.spotify.com/v1/',

  GET_ME_SUCCESS: 'GET_ME_SUCCESS',
  GET_ME_ERROR: 'GET_ME_ERROR',
  GET_MY_PLAYLISTS_SUCCESS: 'GET_MY_PLAYLISTS_SUCCESS',
  GET_MY_PLAYLISTS_ERROR: 'GET_MY_PLAYLISTS_ERROR',

  GET_USER_SUCCESS: 'GET_USER_SUCCESS',
  GET_USER_ERROR: 'GET_USER_ERROR',

  SHOW_LOADER: 'SHOW_LOADER',
  HIDE_LOADER: 'HIDE_LOADER',

  GET_ARTIST_SUCCESS: 'GET_ARTIST_SUCCESS',
  GET_ARTIST_ERROR: 'GET_ARTIST_ERROR',
  GET_ARTIST_TOP_TRACKS_SUCCESS: 'GET_ARTIST_TOP_TRACKS_SUCCESS',
  GET_ARTIST_ALBUMS_SUCCESS: 'GET_ARTIST_ALBUMS_SUCCESS',

  GET_PLAYLISTS_SUCCESS: 'GET_PLAYLISTS_SUCCESS',
  GET_PLAYLISTS_ERROR: 'GET_PLAYLISTS_ERROR',
  GET_PLAYLIST_SUCCESS: 'GET_PLAYLIST_SUCCESS',
  GET_PLAYLIST_ERROR: 'GET_PLAYLIST_ERROR',

  GET_TRACK_SUCCESS: 'GET_TRACK_SUCCESS',
  GET_TRACK_ERROR: 'GET_TRACK_ERROR',

  GET_ALBUM_SUCCESS: 'GET_ALBUM_SUCCESS',
  GET_ALBUM_ERROR: 'GET_ALBUM_ERROR',
  GET_ALBUM_TRACKS_SUCCESS: 'GET_ALBUM_TRACKS_SUCCESS',
  GET_ALBUM_TRACKS_ERROR: 'GET_ALBUM_TRACKS_ERROR',

  GET_NEW_RELEASES_SUCCESS: 'GET_NEW_RELEASES_SUCCESS',
  GET_NEW_RELEASES_ERROR: 'GET_NEW_RELEASES_ERROR',

  GET_FEATURED_PLAYLISTS_SUCCESS: 'GET_FEATURED_PLAYLISTS_SUCCESS',
  GET_FEATURED_PLAYLISTS_ERROR: 'GET_FEATURED_PLAYLISTS_ERROR',

  GET_BROWSE_CATEGORIES_SUCCESS: 'GET_BROWSE_CATEGORIES_SUCCESS',
  GET_BROWSE_CATEGORIES_ERROR: 'GET_BROWSE_CATEGORIES_ERROR',
  GET_BROWSE_CATEGORY_PLAYLISTS_SUCCESS: 'GET_BROWSE_CATEGORY_PLAYLISTS_SUCCESS',
  GET_BROWSE_CATEGORY_PLAYLISTS_ERROR: 'GET_BROWSE_CATEGORY_PLAYLISTS_ERROR',
};

export default actionTypes;
