import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import Profile from '../components/Profile';
import Track from '../components/Track';
import { getAlbum, getAlbumTracks } from '../actions/album';

class AlbumPage extends PureComponent {
  constructor(props) {
    super(props);

    this.renderAlbumProfile = this.renderAlbumProfile.bind(this);
    this.renderAlbumTracks = this.renderAlbumTracks.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.match.params.albumId !== this.props.match.params.albumId) {
      this.props.getAlbum(nextProps.match.params.albumId)
      this.props.getAlbumTracks(nextProps.match.params.albumId)
    }
  }

  componentDidMount() {
    const albumId = this.props.match.params.albumId;

    this.props.getAlbum(albumId)
    this.props.getAlbumTracks(albumId)
  }

  renderAlbumProfile() {
    const album = this.props.album;

    let href = album.artists[0].href || '';
    let cutFrom = href.split('/').indexOf("v1") + 1;
    let link = '/' + href.split('/').slice(cutFrom).join('/');
    
    return <Profile id={ album.id } 
                    image={ album.images[1].url } 
                    name={ album.name } 
                    type={ album.album_type } 
                    author={ album.artists[0].name } 
                    authorLink={ link }/>
  }

  renderAlbumTracks() {
    const tracks = this.props.albumTracks.items;

    return tracks.map((track, index) => {

      return <Track key={ track.id }
                    id={ track.id } 
                    index={ index } 
                    name={ track.name }
                    duration={ track.duration_ms }
                    preview_url={ track.preview_url }/>})
    
  }

  render() {
    return (
      <div>
        { Object.keys(this.props.album).length !== 0 && this.renderAlbumProfile() }
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>SONG</th>
              <th>TIME</th>
            </tr>
          </thead>
          <tbody>
          { Object.keys(this.props.albumTracks).length !== 0 && this.renderAlbumTracks() }
          </tbody>
        </table>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({ 
    album: state.album,
    albumTracks: state.albumTracks,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getAlbum: (albumId) => dispatch(getAlbum(albumId)),
    getAlbumTracks: (albumId) => dispatch(getAlbumTracks(albumId)),
  };
}

const AlbumPageContainer = connect(mapStateToProps, mapDispatchToProps)(AlbumPage);

export default AlbumPageContainer;