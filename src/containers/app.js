import React from 'react';
import LoaderContainer from './loader';
import Header from '../components/Header';
import SidebarContainer from './sidebar';

const App = ({ children }) => (
  <div className="app-wrapper">
    <Header />
    <div className="content-wrapper">
      <SidebarContainer />
      { children }
    </div>
    <LoaderContainer />
  </div>
);

export default App;