import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import Profile from '../components/Profile';
import Cover from '../components/Cover';
import Track from '../components/Track';
import { getArtist, getArtistTopTracks, getArtistAlbums } from '../actions/artist';

class ArtistPage extends PureComponent {
  constructor(props) {
    super(props);

    this.renderArtistProfile = this.renderArtistProfile.bind(this);
    this.renderArtistTracks = this.renderArtistTracks.bind(this);
    this.renderArtistAlbums = this.renderArtistAlbums.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.match.params.artistId !== this.props.match.params.artistId) {
      this.props.getArtist(nextProps.match.params.artistId)
      this.props.getArtistTopTracks(nextProps.match.params.artistId)
      this.props.getArtistAlbums(nextProps.match.params.artistId)
    }
  }

  componentDidMount() {
    const artistId = this.props.match.params.artistId;

    this.props.getArtist(artistId)
    this.props.getArtistTopTracks(artistId)
    this.props.getArtistAlbums(artistId)
  }

  renderArtistProfile() {
    const profile = this.props.profile;

    return <Profile id={ profile.id } 
                    image={ profile.images[1].url } 
                    name={ profile.name } 
                    type={ profile.type } 
                    followers={ profile.followers.total }/>
  }

  renderArtistTracks() {
    const tracks = this.props.tracks;

    return tracks.map((track, index) => {
      let href = track.album.href || '';
      let cutFrom = href.split('/').indexOf("v1") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');
      
      return <Track key={ track.id }
                    id={ track.id } 
                    index={ index } 
                    name={ track.name }
                    albumName={ track.album.name }
                    link={ link } 
                    duration={ track.duration_ms }/>})
    
  }

  renderArtistAlbums() {
    const albums = this.props.albums;
    return albums.map(album => {
      let href = album.href || '';
      let cutFrom = href.split('/').indexOf("v1") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');
      
      return <Cover key={ album.id }
                    id={ album.id } 
                    image={ album.images[1].url } 
                    name={ album.name } 
                    link={ link }/>})
  }

  render() {
    return (
      <div>
        { Object.keys(this.props.profile).length !== 0 && this.renderArtistProfile() }
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>SONG</th>
              <th>TIME</th>
              <th>ALBUM</th>
            </tr>
          </thead>
          <tbody>
          { this.props.tracks.length !== 0 && this.renderArtistTracks() }
          </tbody>
        </table>
        <ul className="covers">
          { this.props.albums.length !== 0 && this.renderArtistAlbums() }
        </ul>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({ 
  profile: state.artist.profile,
  tracks: state.artist.tracks,
  albums: state.artist.albums 
});

const mapDispatchToProps = (dispatch) => {
  return {
    getArtist: (artistId) => dispatch(getArtist(artistId)),
    getArtistTopTracks: (artistId) => dispatch(getArtistTopTracks(artistId)),
    getArtistAlbums: (artistId) => dispatch(getArtistAlbums(artistId)),
  };
}

const ArtistPageContainer = connect(mapStateToProps, mapDispatchToProps)(ArtistPage);

export default ArtistPageContainer;