import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import Cover from '../components/Cover';
import { getBrowseCategoryPlaylists } from '../actions/browse';

class CategoryPage extends PureComponent {
  constructor(props) {
    super(props);

    this.renderBrowseCategoryPlaylists = this.renderBrowseCategoryPlaylists.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.match.params.categoryId !== this.props.match.params.categoryId) {
      this.props.getBrowseCategoryPlaylists(nextProps.match.params.categoryId)
    }
  }

  componentDidMount() {
    const categoryId = this.props.match.params.categoryId;

    this.props.getBrowseCategoryPlaylists(categoryId)
  }

  renderBrowseCategoryPlaylists() {
    const playlists = this.props.playlists.items;
    return playlists.map(playlist => {
      let href = playlist.href || '';
      let cutFrom = href.split('/').indexOf("v1") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');

      return <Cover key={ playlist.id }
                    id={ playlist.id } 
                    image={ playlist.images[0].url } 
                    name={ playlist.name } 
                    link={ link }/>})
  }

  render() {
    return (
      <div>
        <h1>{ this.props.match.params.categoryId }</h1>
        <ul className="covers">
          { Object.keys(this.props.playlists).length !== 0 && this.renderBrowseCategoryPlaylists() }
        </ul>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({ 
  playlists: state.browse.categoryPlaylists,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getBrowseCategoryPlaylists: (categoryId) => dispatch(getBrowseCategoryPlaylists(categoryId)),
  };
}

const CategoryPageContainer = connect(mapStateToProps, mapDispatchToProps)(CategoryPage);

export default CategoryPageContainer;