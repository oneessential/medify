import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import Cover from '../components/Cover';
import { getFeaturedPlaylists, getBrowseCategories, getNewReleases } from '../actions/browse';

class HomePage extends PureComponent {
  constructor(props) {
    super(props);

    this.renderFeaturedPlaylists = this.renderFeaturedPlaylists.bind(this);
    this.renderBrowseCategories = this.renderBrowseCategories.bind(this);
    this.renderNewReleases = this.renderNewReleases.bind(this);
  }

  componentDidMount() {
    this.props.getFeaturedPlaylists();
    this.props.getBrowseCategories();
    this.props.getNewReleases();
  }

  renderFeaturedPlaylists() {
    const playlists = this.props.playlists.playlists.items;
    return playlists.map(playlist => {
      let href = playlist.href || '';
      let cutFrom = href.split('/').indexOf("v1") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');

      return <Cover key={ playlist.id }
                    id={ playlist.id } 
                    image={ playlist.images[0].url } 
                    name={ playlist.name } 
                    link={ link }/>})
  }

  renderBrowseCategories() {
    const categories = this.props.categories.items;
    return categories.map(category => {
      let href = category.href || '';
      let cutFrom = href.split('/').indexOf("browse") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');

      return <Cover key={ category.id }
                    id={ category.id } 
                    image={ category.icons[0].url } 
                    name={ category.name } 
                    link={ link }/>})
  }

  renderNewReleases() {
    const releases = this.props.releases.items;
    return releases.map(release => {
      let href = release.href || '';
      let cutFrom = href.split('/').indexOf("v1") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');

      return <Cover key={ release.id }
                    id={ release.id } 
                    image={ release.images[0].url } 
                    name={ release.name } 
                    link={ link }/>})
  }

  render() {
    return (
      <div>
        <h1>Browse</h1>
        <h2>Featured playlists</h2>
        <h3>{ this.props.playlists.message }</h3>
        <ul className="covers">
          { Object.keys(this.props.playlists).length !== 0 && this.renderFeaturedPlaylists() }
        </ul>
        <h2>Genres & Moods</h2>
        <ul className="covers">
          { Object.keys(this.props.categories).length !== 0 && this.renderBrowseCategories() }
        </ul>
        <h2>New releases</h2>
        <ul className="covers">
          { Object.keys(this.props.releases).length !== 0 && this.renderNewReleases() }
        </ul>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({ 
  playlists: state.browse.featuredPlaylists, 
  categories: state.browse.categories, 
  releases: state.browse.newReleases, 
});

const mapDispatchToProps = (dispatch) => {
  return {
    getFeaturedPlaylists: () => dispatch(getFeaturedPlaylists()),
    getBrowseCategories: () => dispatch(getBrowseCategories()),
    getNewReleases: () => dispatch(getNewReleases()),
  };
}

const HomePageContainer = connect(mapStateToProps, mapDispatchToProps)(HomePage);

export default HomePageContainer;