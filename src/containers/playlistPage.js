import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import Profile from '../components/Profile';
import Track from '../components/Track';
import { getPlaylist } from '../actions/playlists';

class PlaylistPage extends PureComponent {
  constructor(props) {
    super(props);

    this.renderPlaylistProfile = this.renderPlaylistProfile.bind(this);
    this.renderPlaylistTracks = this.renderPlaylistTracks.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.match.params.playlistId !== this.props.match.params.playlistId) {
      this.props.getPlaylist(nextProps.match.params.userId, nextProps.match.params.playlistId)
    }
  }

  componentDidMount() {
    const playlistId = this.props.match.params.playlistId;
    const userId = this.props.match.params.userId;

    this.props.getPlaylist(userId, playlistId)
  }

  renderPlaylistProfile() {
    const playlist = this.props.playlist;

    let href = playlist.owner.href || '';
    let cutFrom = href.split('/').indexOf("v1") + 1;
    let link = '/' + href.split('/').slice(cutFrom).join('/');

    return <Profile id={ playlist.id } 
                    image={ playlist.images[0].url } 
                    name={ playlist.name } 
                    type={ playlist.type }
                    description={ playlist.description }
                    followers={ playlist.followers.total > 0 && playlist.followers.total }
                    author={ playlist.owner.id } 
                    authorLink={ link }/>
  }

  renderPlaylistTracks() {
    const tracks = this.props.playlist.tracks.items;

    return tracks.map((t, index) => {
      let track = t.track;

      let href = track.album.href || '';
      let cutFrom = href.split('/').indexOf("v1") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');

      let artistHref = track.album.artists[0].href || '';
      let artistCutFrom = artistHref.split('/').indexOf("v1") + 1;
      let artistLink = '/' + artistHref.split('/').slice(artistCutFrom).join('/');

      return <Track key={ track.id }
                    id={ track.id } 
                    index={ index } 
                    name={ track.name }
                    artist={ track.album.artists[0].name }
                    artistLink={ artistLink }
                    albumName={ track.album.name }
                    link={ link }
                    duration={ track.duration_ms }
                    preview_url={ track.preview_url }/>})
    
  }

  render() {
    return (
      <div>
        { Object.keys(this.props.playlist).length !== 0 && this.renderPlaylistProfile() }
        <table>
          <thead>
            <tr>
              <th>#</th>
              <th>TRACK</th>
              <th>ARTIST</th>
              <th>TIME</th>
              <th>ALBUM</th>
            </tr>
          </thead>
          <tbody>
          { Object.keys(this.props.playlist).length !== 0 && this.renderPlaylistTracks() }
          </tbody>
        </table>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({ 
    playlist: state.playlist,
});

const mapDispatchToProps = (dispatch) => {
  return {
    getPlaylist: (userId, playlistId) => dispatch(getPlaylist(userId, playlistId)),
  };
}

const PlaylistPageContainer = connect(mapStateToProps, mapDispatchToProps)(PlaylistPage);

export default PlaylistPageContainer;