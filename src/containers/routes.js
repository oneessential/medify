import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import App from './app';
import HomePageContainer from './homePage';
import UserPageContainer from './userPage';
import ArtistPageContainer from './artistPage';
import PlaylistPageContainer from './playlistPage';
import AlbumPageContainer from './albumPage';
import CategoryPageContainer from './categoryPage';

const Routes = () => {
  return (
      <App>
        <Switch>
          <Route exact={true} path='/' component={ HomePageContainer } />
          <Route exact={true} path='/users/:userId' component={ UserPageContainer } />
          <Route exact={true} path='/users/:userId/playlists/:playlistId([0-9a-zA-Z]{22})' component={ PlaylistPageContainer } />
          <Route exact={true} path='/artists/:artistId([0-9a-zA-Z]{22})' component={ ArtistPageContainer } />
          <Route exact={true} path='/albums/:albumId([0-9a-zA-Z]{22})' component={ AlbumPageContainer } />      
          <Route exact={true} path='/categories/:categoryId' component={ CategoryPageContainer } />      
          <Redirect to="/"/>
        </Switch>
      </App>
  );
};

export default Routes;