import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { getMe, getMyPlaylists } from '../actions/user';

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.renderUserPlaylists = this.renderUserPlaylists.bind(this);
  }

  componentDidMount() {
    this.props.getMe()
    this.props.getMyPlaylists()
  }

  renderUserPlaylists() {
    const playlists = this.props.playlists.items;

    return playlists.map(playlist => {
      let href = playlist.href || '';
      let cutFrom = href.split('/').indexOf("v1") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');

      return <li key={ playlist.id }><NavLink activeClassName="active" to={ link }>{ playlist.name }</NavLink></li>})
  }

  render() {
    return (
      <div className="sidebar">
        <ul>
          <li><NavLink exact activeClassName="active" to="/">Home Page</NavLink></li>
          <li><NavLink exact activeClassName="active" to="/users/alberte1996">User Page</NavLink></li>
          <li><NavLink activeClassName="active" to="/users/billboard.com/playlists/1E7vWY16oSv3LBjZ9uNzsg">Playlist Page</NavLink></li>
          <li><NavLink activeClassName="active" to="/artists/53XhwfbYqKCa1cC15pYq2q">Artist Page</NavLink></li>        
          <li><NavLink activeClassName="active" to="/albums/2XjPrtaAqvviEgSHCpzHmM">Album Page</NavLink></li>       
          <li><NavLink activeClassName="active" to="/categories/rock">Category Page</NavLink></li>       
        </ul>
        <hr/>
        <h4>Playlists</h4>
        <ul>
          { Object.keys(this.props.playlists).length !== 0 && this.renderUserPlaylists() }
        </ul>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({ 
  profile: state.me.profile, 
  playlists: state.me.playlists, 
});

const mapDispatchToProps = (dispatch) => {
  return {
    getMe: () => dispatch(getMe()),
    getMyPlaylists: () => dispatch(getMyPlaylists()),
  };
}

const SidebarContainer = connect(mapStateToProps, mapDispatchToProps, null, {
  pure: false
})(Sidebar);
export default SidebarContainer;