import React, { PureComponent } from 'react';
import {connect} from 'react-redux';
import Profile from '../components/Profile';
import Cover from '../components/Cover';
import { getUser } from '../actions/user';
import { getPlaylists } from '../actions/playlists';

class UserPage extends PureComponent {
  constructor(props) {
    super(props);

    this.renderUserProfile = this.renderUserProfile.bind(this);
    this.renderUserPlaylists = this.renderUserPlaylists.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.match.params.userId !== this.props.match.params.userId) {
      this.props.getUser(nextProps.match.params.userId)
      this.props.getUserPlaylists(nextProps.match.params.userId)
    }
  }

  componentDidMount() {
    const userId = this.props.match.params.userId;

    this.props.getUser(userId)
    this.props.getUserPlaylists(userId)
  }

  renderUserProfile() {
    const profile = this.props.profile;

    return <Profile id={ profile.id } 
                    image={ profile.images[0].url } 
                    name={ profile.display_name == null ?  profile.display_name : profile.id } 
                    type={ profile.type } 
                    followers={ profile.followers.total > 0 && profile.followers.total }/>
  }

  renderUserPlaylists() {
    const playlists = this.props.playlists;
    return playlists.items.map(playlist => {
      let href = playlist.href || '';
      let cutFrom = href.split('/').indexOf("v1") + 1;
      let link = '/' + href.split('/').slice(cutFrom).join('/');

      return <Cover key={ playlist.id }
                    id={ playlist.id } 
                    image={ playlist.images[0].url } 
                    name={ playlist.name } 
                    link={ link }/>})
  }

  render() {
    return (
      <div>
        { Object.keys(this.props.profile).length !== 0 && this.renderUserProfile() }
        <ul className="covers">
          { Object.keys(this.props.playlists).length !== 0 && this.renderUserPlaylists() }
        </ul>
      </div>
    );
  }
}


const mapStateToProps = (state) => ({ 
  profile: state.user,
  playlists: state.playlists 
});

const mapDispatchToProps = (dispatch) => {
  return {
    getUser: (userId) => dispatch(getUser(userId)),
    getUserPlaylists: (userId) => dispatch(getPlaylists(userId)),
  };
}

const UserPageContainer = connect(mapStateToProps, mapDispatchToProps)(UserPage);

export default UserPageContainer;