import React from 'react';
import ReactDOM from 'react-dom';
import Root from './containers/root';
import configureStore from './store';
import './app.css';

const initialStore = {
    loader: false
};
const store = configureStore(initialStore);

console.log(store.getState())

ReactDOM.render(<Root store={store}/>, document.getElementById('root'));
