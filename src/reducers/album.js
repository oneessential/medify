import ActionTypes from '../constants';

export default function (state = {}, action) {
  switch(action.type) {
    case ActionTypes.GET_ALBUM_SUCCESS:
      return action.payload
    default:
      return state;
  }
}