import ActionTypes from '../constants';

const initialState = {
  profile: {},
  tracks: [],
  albums: []
}

export default function (state = initialState, action) {
  switch(action.type) {
    case ActionTypes.GET_ARTIST_SUCCESS:
      return Object.assign({}, state, {
          profile: action.payload
        })
    case ActionTypes.GET_ARTIST_TOP_TRACKS_SUCCESS:
      return Object.assign({}, state, {
        tracks: action.payload.tracks
      })
    case ActionTypes.GET_ARTIST_ALBUMS_SUCCESS:
      return Object.assign({}, state, {
        albums: action.payload.items
      })
    default:
      return state;
  }
}
 