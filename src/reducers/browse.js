import ActionTypes from '../constants';

const initialState = {
  newReleases: {},
  featuredPlaylists: {},
  categories: {},
  categoryPlaylists: {}
}

export default function (state = initialState, action) {
  switch(action.type) {
    case ActionTypes.GET_NEW_RELEASES_SUCCESS:
      return Object.assign({}, state, {
          newReleases: action.payload.albums
        })
    case ActionTypes.GET_FEATURED_PLAYLISTS_SUCCESS:
      return Object.assign({}, state, {
        featuredPlaylists: action.payload
      })
    case ActionTypes.GET_BROWSE_CATEGORIES_SUCCESS:
      return Object.assign({}, state, {
        categories: action.payload.categories
      })
    case ActionTypes.GET_BROWSE_CATEGORY_PLAYLISTS_SUCCESS:
      return Object.assign({}, state, {
        categoryPlaylists: action.payload.playlists
      })
    default:
      return state;
  }
}