import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import loader from './loader';
import me from './me';
import user from './user';
import artist from './artist';
import playlists from './playlists';
import playlist from './playlist';
import track from './track';
import album from './album';
import albumTracks from './albumTracks';
import browse from './browse';

const rootReducer = combineReducers({
  router: routerReducer,
  loader,
  me,
  user,
  artist,
  playlists,
  playlist,
  track,
  album,
  albumTracks,
  browse
});

export default rootReducer;
