import ActionTypes from '../constants';

const initialState = {
  profile: {},
  playlists: {}
}

export default function (state = initialState, action) {
  switch(action.type) {
    case ActionTypes.GET_ME_SUCCESS:
      return Object.assign({}, state, {
        profile: action.payload
      })
    case ActionTypes.GET_MY_PLAYLISTS_SUCCESS:
      return Object.assign({}, state, {
        playlists: action.payload
      })
    default:
      return state;
  }
}
