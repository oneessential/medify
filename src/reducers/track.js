import ActionTypes from '../constants';

export default function (state = null, action) {
  switch(action.type) {
    case ActionTypes.GET_TRACK_SUCCESS:
      return action.payload
    default:
      return state;
  }
}