export {
  formatDuration
};

function digits(d) {
    if(d < 10) {
        return '0' + d;
    } else {
        return d;
    }
}

function formatDuration(miliseconds) {
    if(miliseconds) {
        let seconds = Math.round(+miliseconds) / 1000;
        let minutes = Math.floor(seconds / 60);
        seconds = Math.round(seconds - minutes * 60);
        let hours = Math.floor(minutes / 60);
        minutes = Math.round(minutes - hours * 60);

        if(hours > 0) {
            return digits(hours) + ':' + digits(minutes) + ':' + digits(seconds);
        } else {
            return digits(minutes) + ':' + digits(seconds);
        }
    } else {
        return '';
    }
}
