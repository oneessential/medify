import { tokenAlive } from './token';

export {
  get
};

function get(url) {
    if(!tokenAlive()) {
        //redirect to login page
        console.log('TOKEN EXPIRED')
        function login(callback) {
            var CLIENT_ID = '03d11eee5e8247bdb15ad1ba08b1fee4';
            var REDIRECT_URI = 'https://oneessential.gitlab.io/medify/callback.html';
            function getLoginURL(scopes) {
                return 'https://accounts.spotify.com/authorize?client_id=' + CLIENT_ID +
                    '&redirect_uri=' + encodeURIComponent(REDIRECT_URI) +
                    '&scope=' + encodeURIComponent(scopes.join(' ')) +
                    '&response_type=token';
            }
            
            var url = getLoginURL([
                'user-read-private',
                'playlist-read-private',
                'playlist-modify-public',
                'playlist-modify-private',
                'user-library-read',
                'user-library-modify',
                'user-follow-read',
                'user-follow-modify'
            ]);
            
            var width = 450,
                height = 730,
                left = (window.screen.width / 2) - (width / 2),
                top = (window.screen.height / 2) - (height / 2);
        
            window.addEventListener("message", function(event) {
                var hash;
                try {
                    hash = JSON.parse(event.data);
                    if (hash.type === 'access_token') {
                        console.log(hash.access_token);
                    }
                } catch (error) {
                    return null;
                }         
            }, false);
            
            window.open(
                url,
                'Spotify',
                'menubar=no,location=no,resizable=no,scrollbars=no,status=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left
            );  
            
        }

        login();
    }

    const accessToken = localStorage.getItem('access_token');
    let myHeaders = new Headers();
    myHeaders.append("Authorization", 'Bearer ' + accessToken);
    let myInit = { 
        method: 'GET',
        headers: myHeaders,
        mode: 'cors',
        cache: 'default' 
    };
  return fetch(new Request(url, myInit));
}