export {
  expiredTime,
  tokenAlive
};

function expiredTime(lifetime) {
    let time = new Date();
    time = new Date(time.getTime() + lifetime * 10)
    return +time
}

function tokenAlive() {
    let token = localStorage.getItem('access_token');
    let expires = localStorage.getItem('token_expires');

    if(token) {
        if(new Date().getTime() < expires) {
            return true;
        }
        return false;
    } else {
        return false;
    }
}

